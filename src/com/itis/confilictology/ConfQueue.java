package com.itis.confilictology;

import java.util.Arrays;
import java.util.Random;

public class ConfQueue {

    private static String[] group = { "Артур", "Булат", "Денис", "Ильсур", "Олег", "Тимур" };

    public static void main(String[] args) {

        String[] resultQueue = null;
        for (int i = 0; i < Math.pow(6, 6); i++) {
            resultQueue = goRandom();

        }
        System.out.println(Arrays.toString(resultQueue));
    }

    // первые 2 человека в списке делают вместе 2 пункт. Остальные по порядку берут соответствующий пункт
    // то есть 3-ий в списке берет 1-ый пункт, 4-ый в списке - 3-ий пункт и так далее
    private static String[] goRandom() {
        String[] workGroup = new String[group.length];
        Random r = new Random();

        // random queue
        int i = 0;
        while (i < group.length) {
            int numInQueue = r.nextInt(group.length + 1);
            if (numInQueue == group.length) {
                numInQueue = r.nextInt(group.length);
            }
            if (isCorrect(group[numInQueue], workGroup)) {
                workGroup[i] = group[numInQueue];
                i++;
            }
        }
        return workGroup;
    }

    private static boolean isCorrect(String name, String[] workGroup) {
        boolean correct = true;
        for (int i = 0; i < workGroup.length && correct; i++) {
            if (workGroup[i] != null && workGroup[i].equals(name)) {
                correct = false;
            }

        }
        return correct;
    }

}
